# LED Style Gain Meter for QSYS

###### LED Gain Meter with dBFS input for setting -20 dBFS nominal gain.  

## Overview

QSYS system User Component is designed as a user facing gain setting meter to be embedded in UCI designs.  It takes any dBFS peak source as input and displays the level by illuminating a series of 5 LEDs reflecting a range of sensed values.  LEDs can be extracted from the code block and displayed as LEDs or buttons of any desired color to help users set an appropriate gain into the QSYS ecosystem for a changeable input source.  This is a similar approach to the simple metering offered by many console mixers including the TF series by Yamaha.  

## Installation

By default, QSYS Designer stores user components in the document folder of the user name.  

*C:\Users\user-name\Documents\QSC\Q-Sys Designer\User Components*

Copy the *.quc file to this folder for it to be immediately installed under User Components in Designer.  

See q-system help topic "User Components" for further information.

![installed_user_component](./graphics/installed_user_component.PNG)

## Use and Method

A dBFS meter source is attached to the input pin.  This is most often accomplished by exposing the level (dBFS) pin from a core input channel, although any dBFS source can be used.  

The source dBFS is evaluated every 0.25 seconds and the corresponding LED is illuminated or de-illuminated in response to the level sensed.  The threshold is as follows:

| LED  | Range                 |
| ---- | --------------------- |
| 1    | < -38 dBFS            |
| 2    | >= -38 and < -23 dBFS |
| 3    | >= -23 and < -17 dBFS |
| 4    | >= -17 and < -2 dBFS  |
| 5    | >=  -2 dBFS           |

Target nominal level  = -20 dBFS  which is approximately +4 dBU

Drag out LEDs from control block for use in UCIs directly, or convert LED to button to mimic the function often found on traditional mixing councils.  

![](./graphics/screenshot.PNG)



## Developer

Jason Gottsacker
jgottsacker@soundplanning.com